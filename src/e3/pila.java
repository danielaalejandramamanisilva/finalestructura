/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e3;

/**
 *
 * @author Nombre: Daniela Alejandra Mamani Silva   CI:85377754
 */
public class pila<T> {
    private nodo<T> primero;
    private nodo<T> ultimo;
    private int tamaño;

    public pila() {
        this.primero = null;
        this.ultimo = null;
        this.tamaño = 0;
    }

    public boolean vacio() {
        return tamaño == 0;
    }

    public int tamañolista() {
        return tamaño;
    }

    private nodo<T> getNode(int index) {
        if (vacio() || (index < 0 || index >= tamañolista())) {
            return null;
        } else if (index == 0) {
            return primero;
        } else if (index == tamañolista() - 1) {
            return ultimo;
        } else {
            nodo<T> search = primero;
            int contador = 0;
            while (contador < index) {
                contador++;
                search = search.getNext();
            }
            return search;
        }
    }

    public T sacar(int index) {
        if (vacio() || (index < 0 || index >= tamañolista())) {
            return null;
        } else if (index == 0) {
            return primero.getElement();
        } else if (index == tamañolista() - 1) {
            return ultimo.getElement();
        } else {
            nodo<T> search = getNode(index);
            return search.getElement();
        }
    }

    public T sacarprimero() {
        if (vacio()) {
            return null;
        } else {
            return primero.getElement();
        }
    }

    public T sacarultimo() {
        if (vacio()) {
            return null;
        } else {
            return ultimo.getElement();
        }
    }

    public T agregarprimero(T element) {
        nodo<T> newelement;
        if (vacio()) {
            newelement = new nodo<>(element, null);
            primero = newelement;
            ultimo = newelement;
        } else {
            newelement = new nodo<T>(element,primero);
            primero = newelement;
        }
        tamaño++;
        return primero.getElement();
    }

    public T agregarultimo(T element) {
        nodo<T> newelement;
        if (vacio()) {
            return agregarprimero(element);
        } else {
            newelement = new nodo<T>(element,null);
            ultimo.setNext(newelement);
            ultimo = newelement;
        }
        tamaño++;
        return ultimo.getElement();
    }

    public T agregarcualquierpos(T element, int index) {//medio
        if (index == 0) {
            return agregarprimero(element);
        } else if(index == tamañolista()){
            return agregarultimo(element);
        }else if((index<0 || index >= tamañolista())){
            return null;
        }else{
            nodo<T> nodo_prev = getNode(index - 1);
            nodo<T> nodo_current = getNode(index);
            nodo<T> newelement = new nodo<>(element,nodo_current);
            nodo_prev.setNext(newelement);
            tamaño++;
            return getNode(index).getElement();
        }
    }
    
    public String contenidodelista(){
        String str = "";
        if(vacio()){
            str = "La lista esta vacia";
        }else{
           nodo<T> out = primero;
           while(out != null){
               str += out.getElement()+" - ";
               out = out.getNext();
           }
        }
        return str;
    }
    
    public T eliminarprimero(){
        if(vacio()){
            return null;
        }else{
            T element = primero.getElement();
            nodo<T> aux = primero.getNext();
            primero = aux;
            if(tamañolista() == 1){
                ultimo = null;
            }
            tamaño--;
            return element;
        }
    }
    
    public T eliminarultimo(){
        if(vacio()){
            return null;
        }else{
            T element = ultimo.getElement();
            nodo<T> newLast = getNode(tamañolista()-2);
            if(newLast == null){
                ultimo = null;
                if(tamañolista() == 2){
                    ultimo = primero;
                }else{
                    primero = null;
                }
            }else{
                ultimo = newLast;
                ultimo.setNext(null);
            }
            tamaño--;
            return element;
        }
    }
}
    
    
    

