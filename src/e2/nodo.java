/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e2;

/**
 *
 * @author Nombre: Daniela Alejandra Mamani Silva   CI:85377754
 */
public class nodo<T> {
    private T elemento;
    private nodo<T> siguiente;
    
    public nodo(T elemento, nodo<T> siguiente){
        this.elemento = elemento;
        this.siguiente = siguiente;
    }

    public T getElemento() {
        return elemento;
    }

    public void setElemento(T elemento) {
        this.elemento = elemento;
    }

    public nodo<T> getSiguiente() {
        return siguiente;
    }

    public void setSiguiente(nodo<T> siguiente) {
        this.siguiente = siguiente;
    }
    
}