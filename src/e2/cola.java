/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e2;

/**
 *
 * @author Nombre: Daniela Alejandra Mamani Silva   CI:85377754
 */
public class cola<T>{
     private nodo<T> primero;
    private nodo<T> ultimo;
    private int tamanio;
    
    public cola(){
        this.primero = null;
        this.ultimo = null;
        tamanio = 0;
    }
    
    public boolean estaVacio(){
        return primero == null;
    }
    
    public int tamanioCola(){
        return tamanio;
    }
    
    public T primerelemento(){
        if(estaVacio()){
            return null;
        }
        return primero.getElemento();
    }
    
    public void introducirDato(T elemento){
        nodo<T> nuevo = new nodo(elemento,null);
        if(estaVacio()){
            primero = nuevo;
            ultimo = nuevo;
        }else{
            if(tamanioCola() == 1){
                primero.setSiguiente(nuevo);
            }else{
                ultimo.setSiguiente(nuevo);
            }
            ultimo = nuevo;
        }
        tamanio++;
    }
    
    public T sacarDato(){
        if(estaVacio()){
            return null;
        }
        
        T elemento = primero.getElemento();
        nodo<T> aux = primero.getSiguiente();
        primero = aux;
        tamanio--;
        if(estaVacio()){
            ultimo = null;
        }
        return elemento;
    }
}
