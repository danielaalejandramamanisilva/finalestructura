/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package e1;

/**
 *
 * @author Nombre: Daniela Alejandra Mamani Silva   CI:85377754
 */
public class pila {
    char arr[];
    int top;
    
    pila(){
        this.arr = new char[15];
        this.top = -1;
    }
    public boolean vacio(){
        return (top == -1);
    }
    public void agregar(char element){
        top++;
        if(top < arr.length){
            arr[top] = element;
        }else{
            char temp[] = new char[arr.length+5];
            
            for(int i = 0; i<arr.length; i++){
                temp[i] = arr[i];
            }
            
            arr = temp; 
            arr[top] = element;
        }
    }
    
    public char cima(){
        return arr[top];
    }
    
    public char sacar(){
        if(!vacio()){
            int temptop = top;
            top--;
            char returntop = arr[temptop];
            return returntop;
        }else{
            return '-';
        }
    }

}
