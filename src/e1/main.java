package e1;
/**
 *
 * @author Nombre: Daniela Alejandra Mamani Silva   CI:85377754
 */
public class main {
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String palabra = "Daniela";
        pila palindromo = new pila();
        for(int i=0;i<palabra.length();i++){
            palindromo.agregar(palabra.charAt(i)); 
        }
        String controlador = "";
        while(!palindromo.vacio()){
            controlador = controlador+palindromo.sacar();
        }
        if(palabra.equals(controlador)){
            System.out.println("Es un palindromo");
        }else{
            System.out.println("No es palindromo");
        }
    }
}
